#!/bin/bash

set -e

# ensure number of cpu threads environment variable defined
if [ "$(echo $CPU)" == "" ]; then CPU=$(sysctl -n hw.ncpu); fi

# build from source
pushd src
make -j $CPU
popd

# create binaries
mv src/lazycpp bin/lazycpp
pushd bin
ln -sf lazycpp lzz
popd
