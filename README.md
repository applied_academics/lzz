# Lazy C++

## Build
Perform the following steps to build the `lzz` binary.

```sh
./build.sh
```

## Environment
Ensure the `lzz` binary in the `bin/` directory is placed in the `$PATH` environment variable.

```sh
cp bin/lzz /usr/local/bin/lzz
```
